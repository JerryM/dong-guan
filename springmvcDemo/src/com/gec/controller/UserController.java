package com.gec.controller;

import com.gec.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//标志注解，用于标明当前类是控制的代码，交给ioc容器管理的
@Controller
public class UserController {

    //   http://localhost:8080/springmvcDemo/login?name=admin&password=123456
    //定义请求处理的方法【登录请求处理】【方法级别】
//    @RequestMapping("/login")
//    @ResponseBody  //意思：直接返回数据，输出到页面中，不跳转页面
//    public String login(String name, String password) {
//        //
//        if (name.equals("admin") && password.equals("123456")) {
//            return "login success!";
//        } else {
//            return "login failed!";
//        }
//    }

//    //页面跳转的请求处理方法
//    @RequestMapping("/login")
//    public String login(String name, String password) {
//        //登录成功
//        if (name.equals("admin") && password.equals("123456")) {
//            return "redirect:/index.html";//  重定向  不走视图解析器的
//        } else {//登录失败
//            return "login";//  走视图解析器的   /login.jsp
//        }
//    }

    //页面跳转的请求处理方法   springmvc对请求提交的数据进行封装处理   请求提交的key要和实体类的成员变量名要一样，不一样则拿不到数据
    @RequestMapping("/login")
    public String login(User user) {
        //登录成功
        if (user.getName().equals("admin") && user.getPassword().equals("123456")) {
            return "redirect:/index.html";//  重定向  不走视图解析器的
        } else {//登录失败
            return "login";//  走视图解析器的   /login.jsp
        }
    }

}
