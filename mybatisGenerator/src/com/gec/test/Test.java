package com.gec.test;

import com.gec.bean.User;
import com.gec.bean.UserExample;
import com.gec.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException {
        //1、加载mybatis的全局配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //2、生成sqlsessionfactory【会话的工厂类】【用于生成session会话】
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3、通过sqlsessionfactory来得到生成sqlsession会话对象
        SqlSession session = sqlSessionFactory.openSession();
        //4、通过会话对象得到mapper接口对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        //通过mapper对象执行操作方法
        User user = new User();
        user.setPassword("sdf");
        user.setIdcard("7654344455643");
        //insert()插入表中的所有字段  insertSelective()有选择性的插入字段【不为空的】
//        int result = userMapper.insert(user);
//        System.out.println(result);

        //根据主键进行修改操作updateByPrimaryKey()
//        int result = userMapper.updateByPrimaryKeySelective(user);
//        System.out.println(result);
        //根据模板进行操作
        UserExample example = new UserExample();
        //使用example中的内部类来添加操作条件
        example.createCriteria().andUsernameEqualTo("li");
        //   第一个参数表示为要修改的对象数据   第二个参数表示为修改条件
        int result = userMapper.updateByExampleSelective(user,example);
        System.out.println(result);

        //根据主键删除数据
//        int result = userMapper.deleteByPrimaryKey(6);
//        System.out.println(result);

        //根据模板进行操作
//        UserExample example = new UserExample();
//        //使用example中的内部类来添加操作条件
//        example.createCriteria().andUsernameEqualTo("tom");
//        //用example进行操作
//        int result = userMapper.deleteByExample(example);
//        System.out.println(result);

        //添加删除修改要提交事务
        session.commit();


    }

}
