package com.test;

import com.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    //IOC容器管理对象好处：优化性能、对象获取（重复利用）
    public static void main(String[] args) {
        //1、获取Ioc容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        //2、在IOC容器中获取Javabean【对象】
        User user1 = (User) context.getBean("nowUser");
        User user2 = (User) context.getBean("nowUser");
        //在ssm项目整合中。可以直接使用注解获取并注入对象
//        @Autowired
//        User user;
        //
        System.out.println(user1);
        System.out.println(user2);
        //spring框架中，默认是单例
        System.out.println(user1==user2);
    }
}
