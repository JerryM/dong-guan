<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/4/17
  Time: 16:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--管理员的菜单--%>
<c:if test="${nowAdmin!=null}">
    <!-- begin::side menu -->
    <div class="side-menu">
        <div class='side-menu-body'>
            <ul>
                <li class="side-menu-divider">Navigation</li>
                <li class="open">
                    <a href="#"><i class="icon ti-home"></i> <span>用户模块</span> </a>
                    <ul>
                        <li><a href="queryUser">用户列表</a></li>
                        <li><a href="UserInsert.jsp">添加用户</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-rocket"></i> <span>银行模块</span> </a>
                    <ul>
                        <li><a href="queryBank">银行列表</a></li>
                        <li><a href="BankInsert.jsp">添加银行</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-layers-alt"></i> <span>银行卡模块</span> </a>
                    <ul>
                        <li><a href="queryBankCard">银行卡列表</a></li>
                        <li><a href="toInsertBankCard">添加银行卡</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <!-- end::side menu -->
</c:if>

<%--前台用户的菜单--%>
<c:if test="${nowUser!=null}">
    <!-- begin::side menu -->
    <div class="side-menu">
        <div class='side-menu-body'>
            <ul>
                <li class="side-menu-divider">Navigation</li>
                <li class="open">
                    <a href="#"><i class="icon ti-home"></i> <span>用户模块</span> </a>
                    <ul>
                        <li><a href="UserUpdate.jsp">个人中心</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-rocket"></i> <span>银行模块</span> </a>
                    <ul>
                        <li><a href="queryBank">银行列表</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-layers-alt"></i> <span>银行卡模块</span> </a>
                    <ul>
                        <li><a href="queryUserBankCard">银行卡列表</a></li>
                        <li><a href="toInsertUserBankCard">添加银行卡</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-paint-roller"></i> <span>基金模块</span> </a>
                    <ul>
                        <li><a href="queryFundProduct">基金列表</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon ti-clipboard"></i> <span>基金投资模块</span> </a>
                    <ul>
                        <li><a href="queryUserFundProduct">基金投资列表 </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- end::side menu -->
</c:if>
