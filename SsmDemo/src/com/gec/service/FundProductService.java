package com.gec.service;

import com.gec.bean.FundProduct;

import java.util.List;

public interface FundProductService {

    //基金添加功能
    public boolean insert(FundProduct FundProduct);

    //基金列表查询功能
    public List<FundProduct> query();

    //基金根据id查询功能
    public FundProduct queryId(int id);

    //基金根据id修改功能
    public boolean updateFundProduct(FundProduct FundProduct);

    //根据主键删除基金功能
    public boolean delete(int id);
}
