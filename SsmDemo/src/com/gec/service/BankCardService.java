package com.gec.service;

import com.gec.bean.Bankcard;

import java.util.List;

//业务层的抽象接口及方法定义
public interface BankCardService {

    //银行注册功能
    public boolean insert(Bankcard Bankcard);

    //银行列表查询功能
    public List<Bankcard> query();

    //银行根据id查询功能
    public Bankcard queryId(int id);

    //银行根据id修改功能
    public boolean updateBankcard(Bankcard Bankcard);

    //根据主键删除银行功能
    public boolean delete(int id);

}
