package com.gec.service.impl;

import com.gec.bean.FundProductExample;
import com.gec.bean.UserFundProduct;
import com.gec.bean.UserFundProductExample;
import com.gec.mapper.UserFundProductMapper;
import com.gec.service.UserFundProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserFundProductServiceImpl implements UserFundProductService {


    @Autowired
    UserFundProductMapper userFundProductMapper;

    @Override
    public boolean insert(UserFundProduct UserFundProduct) {
        return userFundProductMapper.insert(UserFundProduct) > 0 ? true : false;
    }

    @Override
    public List<UserFundProduct> query() {
        return userFundProductMapper.selectByExample(new UserFundProductExample());
    }

    @Override
    public UserFundProduct queryId(int id) {
        return null;
    }

    @Override
    public boolean updateUserFundProduct(UserFundProduct UserFundProduct) {
        return false;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }
}
