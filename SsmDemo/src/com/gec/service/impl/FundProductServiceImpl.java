package com.gec.service.impl;

import com.gec.bean.BankExample;
import com.gec.bean.FundProduct;
import com.gec.bean.FundProductExample;
import com.gec.mapper.FundProductMapper;
import com.gec.service.FundProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FundProductServiceImpl implements FundProductService {

    @Autowired
    FundProductMapper fundProductMapper;

    @Override
    public boolean insert(FundProduct FundProduct) {
        return false;
    }

    @Override
    public List<FundProduct> query() {
        return fundProductMapper.selectByExample(new FundProductExample());
    }

    @Override
    public FundProduct queryId(int id) {
        return null;
    }

    @Override
    public boolean updateFundProduct(FundProduct FundProduct) {
        return false;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }
}
