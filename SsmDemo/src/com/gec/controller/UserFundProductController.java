package com.gec.controller;

import com.gec.bean.Bank;
import com.gec.bean.User;
import com.gec.bean.UserFundProduct;
import com.gec.service.BankService;
import com.gec.service.UserFundProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UserFundProductController {

    @Autowired
    UserFundProductService userFundProductService;

    //定义查询操作的请求方法
    @RequestMapping("/queryUserFundProduct")
    public String queryUserFundProduct(@RequestParam(required = false, defaultValue = "1") Integer pageNo, HttpServletRequest request) {
        //调用业务层进行查询，并且得到结果
        //开启分页操作   当页页码，每页显示记录条数
        PageHelper.startPage(pageNo, 3);
        List<UserFundProduct> list = userFundProductService.query();
        //生成page分页模型的信息
        PageInfo pageInfo = new PageInfo(list);
        //把信息保存在请求作用域中，目的给jsp页面获取并显示
        request.setAttribute("list", list);
        request.setAttribute("pageInfo", pageInfo);
        //跳转到jsp页面获取并显示
        return "UserFundProductList";
        // return list;
    }

    //定义添加操作的请求方法
    @RequestMapping("/insertUserFundProduct")
    public String insertUserFundProduct(UserFundProduct UserFundProduct, HttpServletRequest request) {
        UserFundProduct.setStatus(1);
        //调用业务层进行添加
        boolean isok = userFundProductService.insert(UserFundProduct);
        if (isok) {
            return "redirect:/queryUserFundProduct";
        } else {
            //把信息保存在请求作用域中，目的给jsp页面获取并显示
            request.setAttribute("message", "添加失败，请重新操作！");
            return "error";
        }
    }

}
